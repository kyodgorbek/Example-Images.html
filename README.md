# Example-Images.html
<html>
    <head>
	<title>Images</title>
   </head>
	<body>
	   <h1>
	      <img src="images/logo.gif"
	                    alt="From A to Zucchini"  />
	  </h1>
	  <figure>
	     <img src="images/chocolate-islands.jpg"
	               alt="Chocolate Islands"
		       title="Chocolate Islands Individual Cakes" />
	     <p>
	         <figcaption>
		   This recipe for individual chocolate
			   cake is  so simple and so delectable!
		</figcaption>
	   </p>
	</figure>
	<h4>More Recepies:</h4>
	<p>
	     <img src="images/lemon-posset.jpg"
		               alt="Lemon Posset"
		               title="Lemon Posset Desert"  />
	     <img src="images/roasted-brussel-sprouts.jpg"
		       alt="Roasted Brussel Sprouts"
		        title="Roasted Brussel Sprouts Side Dish" />
	     <img src="images/zucchini-cake.jpg"
		               alt="Zuchini Cake"
				title="Zuchini Cake No Frosting" />
   		   
	</p>
   </body>
</html>
